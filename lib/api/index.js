var express = require('express');
var handler = require('./handler');

var router = express.Router();

router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api!' });
});

router.post('/mocks', handler.newMock);
router.get('/mocks/:id', handler.findMockById);
router.delete('/mocks/:id', handler.deleteMock);
router.put('/mocks/:id', handler.updateMock);
router.get('/mocks', handler.findMocks);

module.exports = router;