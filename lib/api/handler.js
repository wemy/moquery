var mockRepository = require('../mockRepository');

module.exports.newMock = function (req, res) {
    var mock = req.body;
    mock.enabled = true;

    mockRepository.insert(mock).
        then(function (createdMock) {
            res.status(201).
                send(createdMock);
        }).catch(function(error){
            res.status(500).
                json(error);
        });
};

module.exports.findMocks = function (req, res) {
    mockRepository.findAll().
        then(function (mocks) {
            res.status(200).
                send(mocks);
        }).catch(function(error){
            res.status(500).
                json(error);
        });
};

module.exports.findMockById = function (req, res) {
    var id = req.params.id;
    mockRepository.findById(id).
        then(function (mock) {
            res.status(200).
                send(mock);
        }).catch(function(error){
            res.status(500).
                json(error);
        });
};


module.exports.deleteMock = function (req, res) {
    var id = req.params.id;
    mockRepository.remove(id).
        then(function () {
            res.status(204).
                send();
        }).catch(function(error){
            res.status(500).
                json(error);
        });
};

module.exports.updateMock = function (req, res) {
    var id = req.params.id;
    var mock = req.body;

    mockRepository.update(mock).
        then(function () {
            res.status(204).
                send();
        }).catch(function(error){
            res.status(500).
                json(error);
        });
};