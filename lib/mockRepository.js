var Q = require('q');
var Datastore = require('nedb');

var db = new Datastore();

module.exports.insert = function (mock) {
    return Q.ninvoke(db, 'insert', mock);
};

module.exports.findAll = function () {
    return Q.ninvoke(db, 'find', {});
};

module.exports.findById = function (id) {
    return Q.ninvoke(db, 'findOne', { _id: id });
};

module.exports.remove = function (id) {
    return Q.ninvoke(db, 'remove', { _id: id });
};

module.exports.update = function (mock) {
    return Q.ninvoke(db, 'update', { _id: mock._id }, mock, {});
};