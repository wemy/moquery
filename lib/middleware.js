var mockRepository = require('./mockRepository');
var _ = require('lodash');

module.exports = function (req, res, next) {

    mockRepository.findAll().
        then(function (mocks) {
            mocks.sort(function(a, b){
                return a.order - b.order;
            });

            var url = req.url;

            var mock = _.find(mocks, function (mock) {
                var regex = new RegExp(mock.regex);
                return mock.enabled && url.match(regex);
            });

            if (mock) {
                console.log('Mocked url : ' + url);
                var trimmedBody = mock.body.replace(/\r?\n|\r/g, '');
                res.setHeader('Content-Type', mock.contentType);
                res.end(trimmedBody);
            } else {
                next();
            }

        }).catch(function (error) {
            next();
        });
};
