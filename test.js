var express = require('express');
var moquery = require('./index');

var app = express();

app.use(moquery());

app.get('/', function(req, res){
    res.send('hello world');
});

app.listen(5000);