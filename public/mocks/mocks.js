'use strict';

angular.module('moquery.mocks', [
    'ngRoute',
    'moquery.api'
])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/mocks', {
            templateUrl: 'mocks/mocks.html',
            controller: 'MocksCtrl',
            controllerAs: 'MocksCtrl',
            resolve: {
                mocks: function(Mocks){
                    return Mocks.getList();
                }
            }
        });
    }])

    .controller('MocksCtrl', function (Mocks, mocks, $filter, $scope) {

        this.mock = {};
        this.mocks = $filter('orderBy')(mocks, 'order');

        this.saveMocks = function(){
            _.each(this.mocks, function(mock, index){
                if(index + 1 !== mock.order){
                    mock.order = index + 1;
                    mock.save();
                }
            });
        };

        $scope.$watchCollection(_.bind(function(){
            return this.mocks;
        }, this), _.bind(function(newCollection){
            if(newCollection){
                this.saveMocks();
            }
        }, this));

        this.sortableOptions = {
            axis: 'y'
        };

        this.resetMock = function(){
            this.mock = {};
        };

        this.addMock = function () {
            var mockToSave = _.clone(this.mock);
            mockToSave.order = this.mocks.length + 1;
            Mocks.post(mockToSave).
                then(_.bind(function(savedMock){
                    this.mocks.push(savedMock);
                    this.resetMock();
                }, this));
        };

        this.deleteMock = function(mock, index){
            mock.remove().
                then(_.bind(function(){
                    this.mocks.splice(index, 1);
                }, this));
        };

    });
