'use strict';

angular.module('moquery', [
    'ngRoute',
    'ngAnimate',
    'restangular',
    'ui.sortable',
    'moquery.history',
    'moquery.mocks'
]).
    config(['$routeProvider', function ($routeProvider) {
        $routeProvider.otherwise({redirectTo: '/mocks'});
    }]);