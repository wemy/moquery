'use strict';

angular.module('moquery.api', [
    'restangular',
    'moquery.api.mocks'
]).
    config(function (RestangularProvider) {
        RestangularProvider.setBaseUrl('/api');
        RestangularProvider.setRestangularFields({
            id: "_id"
        });
    });