'use strict';

angular.module('moquery.api.mocks', [
    'restangular'
]).
    factory('Mocks', function (Restangular) {
        return Restangular.service('mocks');
    });