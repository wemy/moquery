var gulp = require('gulp');
var less = require('gulp-less');
var path = require('path');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('less', function () {
    gulp.src('./less/*.less')
        .pipe(sourcemaps.init())
        .pipe(less({
            paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('./public'));
});

gulp.task('default', ['less'], function() {
    // place code for your default task here
});