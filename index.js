var express = require('express');
var bodyParser = require('body-parser');
var api = require('./lib/api');

module.exports = function () {
    var app = express();

    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use(express.static(__dirname + '/public'));
    app.use('/api', api);

    var port = process.env.PORT || 3030;
    app.listen(port);
    console.log('Magic happens on port ' + port);

    return require('./lib/middleware');
};